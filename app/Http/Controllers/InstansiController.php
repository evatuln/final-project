<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\instansi;
use File;

class InstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instansi = instansi::all();
        return view('instansi.index', compact('instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instansi = instansi::all();
        return view('instansi.create', compact('instansi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
            'alamat' => 'required',
            'file_surat' => 'required|mimes:jpeg,png,jpg '
    	]);
 
        instansi::create([
    		'nama' => $request->nama,
            'alamat' => $request->alamat,
            'file_surat' => $request->file_surat
    	]);
 
    	$gambar = $request->file_surat;
        $name_img = time(). ' - '. $gambar->getClientOriginalName();

        instansi::create([
    		'nama' => $request->nama,
            'alamat' => $request->alamat,
            'file_surat' => $name_img
    	]);

        $gambar->move('img', $name_img);
        return redirect('/instansi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instansi = instansi::find($id);
        return view('instansi.show', compact('instansi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instansi = instansi::find($id);
        return view('instansi.edit', compact('instansi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'file_surat' => 'required',
        ]);

        $instansi = instansi::findorfail($id);

        if ($request->has('file_surat')) {

            $path = "/posts";
            File::delete($path . $instansi->file_surat);
            $gambar = $request->file_surat;
            $new_gambar = time(). '-' .$gambar->getClientOriginalName();
            $gambar->move('img', $new_gambar);
            $instansi_data = [
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'file_surat '=> $new_gambar
            ];
        } else {
            $instansi_data = [
                'nama' => $request->nama,
                'alamat' => $request->alamat
            ];
        }
        $instansi->update($instansi_data);

        return redirect('/instansi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instansi = instansi::find($id);
        $instansi->delete();
        return redirect('/instansi');
    }
}
