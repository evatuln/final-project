<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SuratKeluarController extends Controller
{
    public function index()
    {
        $suratkeluar = DB::table('suratkeluar')->get();
        return view('suratkeluar.index', compact('suratkeluar'));
    }

    public function create()
    {
        return view('suratkeluar.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'no_surat' => 'required|unique:suratkeluar', //nama table nya cast
            'tujuan_surat' => 'required',
            'kode_surat' => 'required',
            'isi' => 'required',
            'tanggal_surat' => 'required',
            'tanggal_catat' => 'required'
        ]);
        $query = DB::table('suratkeluar')->insert([
            "no_surat" => $request["no_surat"],
            "tujuan_surat" => $request["tujuan_surat"],
            "kode_surat" => $request["kode_surat"],
            "isi" => $request["isi"],
            "tanggal_surat" => $request["tanggal_surat"],
            "tanggal_catat" => $request["tanggal_catat"],
        ]);
        return redirect('/suratkeluar');
    }

    public function show($id)
    {
        $suratkeluar = DB::table('suratkeluar')->where('id', $id)->first(); // $id menangkap parameternya
        return view('suratkeluar.show', compact('suratkeluar'));
    }

    public function edit($id)
    {
        $suratkeluar = DB::table('suratkeluar')->where('id', $id)->first();
        return view('suratkeluar.edit', compact('suratkeluar'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'no_surat' => 'required', //nama table nya cast
            'tujuan_surat' => 'required',
            'kode_surat' => 'required',
            'isi' => 'required',
            'tanggal_surat' => 'required',
            'tanggal_catat' => 'required'
        ]);

        $query = DB::table('suratkeluar')
            ->where('id', $id)
            ->update([
                "no_surat" => $request["no_surat"],
                "tujuan_surat" => $request["tujuan_surat"],
                "kode_surat" => $request["kode_surat"],
                "isi" => $request["isi"],
                "tanggal_surat" => $request["tanggal_surat"],
                "tanggal_catat" => $request["tanggal_catat"],
            ]);
        return redirect('/suratkeluar');
    }

    public function destroy($id)
    {
        $query = DB::table('suratkeluar')->where('id', $id)->delete();
        return redirect('/suratkeluar');
    }
}
