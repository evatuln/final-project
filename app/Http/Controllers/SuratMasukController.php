<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SuratMasukController extends Controller
{
    public function index()
    {
        $suratmasuk = DB::table('suratmasuk')->get();
        return view('suratmasuk.index', compact('suratmasuk'));
    }

    public function create()
    {
        return view('suratmasuk.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'no_surat' => 'required|unique:suratmasuk', //nama table nya cast
            'asal_surat' => 'required',
            'kode_surat' => 'required',
            'isi' => 'required',
            'tanggal_surat' => 'required',
            'tanggal_terima' => 'required'
        ]);
        $query = DB::table('suratmasuk')->insert([
            "no_surat" => $request["no_surat"],
            "asal_surat" => $request["asal_surat"],
            "kode_surat" => $request["kode_surat"],
            "isi" => $request["isi"],
            "tanggal_surat" => $request["tanggal_surat"],
            "tanggal_terima" => $request["tanggal_terima"],
        ]);
        return redirect('/suratmasuk');
    }

    public function show($id)
    {
        $suratmasuk = DB::table('suratmasuk')->where('id', $id)->first(); // $id menangkap parameternya
        return view('suratmasuk.show', compact('suratmasuk'));
    }

    public function edit($id)
    {
        $suratmasuk = DB::table('suratmasuk')->where('id', $id)->first();
        return view('suratmasuk.edit', compact('suratmasuk'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'no_surat' => 'required', //nama table nya cast
            'asal_surat' => 'required',
            'kode_surat' => 'required',
            'isi' => 'required',
            'tanggal_surat' => 'required',
            'tanggal_terima' => 'required'
        ]);

        $query = DB::table('suratmasuk')
            ->where('id', $id)
            ->update([
                "no_surat" => $request["no_surat"],
                "asal_surat" => $request["asal_surat"],
                "kode_surat" => $request["kode_surat"],
                "isi" => $request["isi"],
                "tanggal_surat" => $request["tanggal_surat"],
                "tanggal_terima" => $request["tanggal_terima"],
            ]);
        return redirect('/suratmasuk');
    }

    public function destroy($id)
    {
        $query = DB::table('suratmasuk')->where('id', $id)->delete();
        return redirect('/suratmasuk');
    }
}
