<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instansi extends Model
{
    protected $table = "instansi";
    protected $fillable = ['nama', 'alamat', 'file_surat'];
    // fillable yang akan diinputkan nantinya
}
