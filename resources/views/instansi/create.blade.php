@extends('layout.master')

@section('content')
<div class="ml-3 mr-3">
    <h2>Tambah Data Instansi</h2>
        <form action="/instansi" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Instansi</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Instansi">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat Surat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat Surat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="file_surat">Scan Surat</label>
                <input type="file" class="form-control" name="file_surat" id="file_surat" placeholder="Masukkan Scan Surat">
                @error('file_surat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection

