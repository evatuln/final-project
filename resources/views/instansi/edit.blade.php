@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <h2>Edit Instansi {{$instansi->id}}</h2>
    <form action="/instansi/{{$instansi->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT') 
        {{-- //method put berfungsi sebagai edit --}}
        <div class="form-group">
            <label for="nama">Nama Instansi</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{$instansi->nama}}" placeholder="Masukkan Nama Instansi">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="alamat">Alamat Instansi</label>
            <input type="text" class="form-control" name="alamat" id="alamat" value="{{$instansi->alamat}}" placeholder="Masukkan Alamat Instansi">
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="file_surat">Scan Surat</label>
            <input type="file" class="form-control" name="file_surat" id="file_surat" value="{{$instansi->file_surat}}" placeholder="Masukkan Scan Surat">
            @error('file_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection