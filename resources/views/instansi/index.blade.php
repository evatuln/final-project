@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
  <a href="/instansi/create" class="btn btn-primary mb-3">Tambah</a>
  <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama Instansi</th>
          <th scope="col">Alamat Instansi</th>
          <th scope="col">File Surat</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
          @forelse ($instansi as $key=>$value)
              <tr>
                  <td>{{$key + 1}}</th>
                  <td>{{$value->nama}}</td>
                  <td>{{$value->alamat}}</td>
                  <td><img  src="{{asset('img/'.$value->file_surat)}}" height="300px" width="200px"></td>
                  <td>
                    <form action="/instansi/{{$value->id}}" method="POST">
                      <a href="/instansi/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                      
                          @csrf
                          @method('DELETE')
                          <input type="submit" class="btn btn-danger my-1" value="Delete">
                      </form>
                  </td>
              </tr>
          @empty
              <tr colspan="3">
                  <td>No data</td>
              </tr>  
          @endforelse              
      </tbody>
  </table>
  </div>
@endsection
@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Masuk ke halaman Instansi",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endpush
