<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
      </div>
      <div class="sidebar-brand-text mx-3">ARSIP SURAT <sup>system</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href="/profil">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Profile</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
      <a class="nav-link" href="/suratmasuk">
        <i class="fas fa-fw fas fa-envelope"></i>
        <span>Surat Masuk</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
      <a class="nav-link" href="/suratkeluar">
        <i class="fas fa-fw fas fa-envelope-open"></i>
        <span>Surat Keluar</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
      <a class="nav-link" href="/instansi">
        <i class="fas fa-fw fas fa-building"></i>
        <span>Instansi</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">

        <i class="fas fa-sign-out-alt"></i>
        {{ __('Logout') }}
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>