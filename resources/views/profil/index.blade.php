@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
          </tr>
        </thead>
        <tbody>
            <td>Arinda Mutyasari dan Nur Evatul Nisa</td>
            <td>20 Tahun</td>
            <td>Kelompok 14 Final Project Sanbercode</td>             
        </tbody>
    </table>
    </div>
  @endsection
  @push('scripts')
  <script>
      Swal.fire({
          title: "Berhasil!",
          text: "Masuk ke halaman Instansi",
          icon: "success",
          confirmButtonText: "Cool",
      });
  </script>
@endsection