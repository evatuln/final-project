@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <h2>Edit Surat Edit {{$suratkeluar->id}}</h2>
    <form action="/suratkeluar/{{$suratkeluar->id}}" method="POST">
        @csrf
        @method('PUT') 
        {{-- //method put berfungsi sebagai edit --}}
        <div class="form-group">
            <label for="no_surat">No Surat</label>
            <input type="text" class="form-control" name="no_surat" id="no_surat" value="{{$suratkeluar->no_surat}}" placeholder="Masukkan No Surat">
            @error('no_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tujuan_surat">Tujuan Surat</label>
            <input type="text" class="form-control" name="tujuan_surat" id="tujuan_surat" value="{{$suratkeluar->tujuan_surat}}" placeholder="Masukkan Asal Surat">
            @error('tujuan_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="kode_surat">Kode Surat</label>
            <input type="text" class="form-control" name="kode_surat" id="kode_surat" value="{{$suratkeluar->kode_surat}}" placeholder="Masukkan Kode Surat">
            @error('kode_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">Isi Surat</label>
            <input type="text" class="form-control" name="isi" id="isi" value="{{$suratkeluar->isi}}" placeholder="Masukkan Isi Surat">
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tanggal_surat">Tanggal Surat</label>
            <input type="date" class="form-control" name="tanggal_surat" id="tanggal_surat" value="{{$suratkeluar->tanggal_surat}}" placeholder="Masukkan Tanggal Surat">
            @error('tanggal_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tanggal_catat">Tanggal Catat</label>
            <input type="date" class="form-control" name="tanggal_catat" id="tanggal_catat" value="{{$suratkeluar->tanggal_catat}}" placeholder="Masukkan Tanggal Terima Surat">
            @error('tanggal_catat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection