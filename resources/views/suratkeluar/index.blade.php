@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
  <a href="/suratkeluar/create" class="btn btn-primary mb-3">Tambah</a>
  <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">No Surat</th>
          <th scope="col">Tujuan Surat</th>
          <th scope="col">Kode Surat</th>
          <th scope="col">Isi</th>
          <th scope="col">Tanggal Surat</th>
          <th scope="col">Tanggal Catat</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
          @forelse ($suratkeluar as $key=>$value)
              <tr>
                  <td>{{$key + 1}}</th>
                  <td>{{$value->no_surat}}</td>
                  <td>{{$value->tujuan_surat}}</td>
                  <td>{{$value->kode_surat}}</td>
                  <td>{{$value->isi}}</td>
                  <td>{{$value->tanggal_surat}}</td>
                  <td>{{$value->tanggal_catat}}</td>
                  <td>
                    <form action="/suratkeluar/{{$value->id}}" method="POST">
                      <a href="/suratkeluar/{{$value->id}}" class="btn btn-info">Show</a>
                      <a href="/suratkeluar/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                      
                          @csrf
                          @method('DELETE')
                          <input type="submit" class="btn btn-danger my-1" value="Delete">
                      </form>
                  </td>
              </tr>
          @empty
              <tr colspan="3">
                  <td>No data</td>
              </tr>  
          @endforelse              
      </tbody>
  </table>
  </div>
@endsection
@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Masuk ke halaman Surat Keluar",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endpush
