@extends('layout.master')

@section('content')
<div class="mt-5 ml-5">
    <h2>Show Data Surat Keluar {{$suratkeluar->id}}</h2>
    <p>{{$suratkeluar->no_surat}}</p>
    <p>{{$suratkeluar->tujuan_surat}}</p>
    <p>{{$suratkeluar->kode_surat}}</p>
    <p>{{$suratkeluar->isi}}</p>
    <p>{{$suratkeluar->tanggal_surat}}</p>
    <p>{{$suratkeluar->tanggal_catat}}</p>
</div>
{{-- <div class="mt-5 ml-5">
    <h2>Show Data Surat Keluar {{$suratkeluar->id}}</h2>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">Nomor Surat</th>
            <th scope="col">Tujuan Surat</th>
            <th scope="col">Kode Surat</th>
            <th scope="col">Isi Surat</th>
            <th scope="col">Tanggal Surat</th>
            <th scope="col">Tanggal Catat</th>
          </tr>
        </thead>
        <tbody>
            <tr>
                <td><p>{{$suratkeluar->no_surat}}</p></td>
                <td><p>{{$suratkeluar->tujuan_surat}}</p></td>
                <td><p>{{$suratkeluar->kode_surat}}</p></td>
                <td><p>{{$suratkeluar->isi}}</p></td>
                <td><p>{{$suratkeluar->tanggal_surat}}</p></td>
                <td><p>{{$suratkeluar->tanggal_catat}}</p></td>
            </tr>
        </tbody>
    </table>
</div> --}}
@endsection