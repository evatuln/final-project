@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <h2>Edit Surat Masuk {{$suratmasuk->id}}</h2>
    <form action="/suratmasuk/{{$suratmasuk->id}}" method="POST">
        @csrf
        @method('PUT') 
        {{-- //method put berfungsi sebagai edit --}}
        <div class="form-group">
            <label for="no_surat">No Surat</label>
            <input type="text" class="form-control" name="no_surat" id="no_surat" value="{{$suratmasuk->no_surat}}" placeholder="Masukkan No Surat">
            @error('no_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="asal_surat">Asal Surat</label>
            <input type="text" class="form-control" name="asal_surat" id="asal_surat" value="{{$suratmasuk->asal_surat}}" placeholder="Masukkan Asal Surat">
            @error('asal_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="kode_surat">Kode Surat</label>
            <input type="text" class="form-control" name="kode_surat" id="kode_surat" value="{{$suratmasuk->kode_surat}}" placeholder="Masukkan Kode Surat">
            @error('kode_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">Isi Surat</label>
            <input type="text" class="form-control" name="isi" id="isi" value="{{$suratmasuk->isi}}" placeholder="Masukkan Isi Surat">
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tanggal_surat">Tanggal Surat</label>
            <input type="date" class="form-control" name="tanggal_surat" id="tanggal_surat" value="{{$suratmasuk->tanggal_surat}}" placeholder="Masukkan Tanggal Surat">
            @error('tanggal_surat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tanggal_terima">Tanggal Terima</label>
            <input type="date" class="form-control" name="tanggal_terima" id="tanggal_terima" value="{{$suratmasuk->tanggal_terima}}" placeholder="Masukkan Tanggal Terima Surat">
            @error('tanggal_terima')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection