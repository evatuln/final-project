@extends('layout.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
  <a href="/suratmasuk/create" class="btn btn-primary mb-3">Tambah</a>
  <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">No Surat</th>
          <th scope="col">Asal Surat</th>
          <th scope="col">Kode Surat</th>
          <th scope="col">Isi</th>
          <th scope="col">Tanggal Surat</th>
          <th scope="col">Tanggal Terima</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
          @forelse ($suratmasuk as $key=>$value)
              <tr>
                  <td>{{$key + 1}}</th>
                  <td>{{$value->no_surat}}</td>
                  <td>{{$value->asal_surat}}</td>
                  <td>{{$value->kode_surat}}</td>
                  <td>{{$value->isi}}</td>
                  <td>{{$value->tanggal_surat}}</td>
                  <td>{{$value->tanggal_terima}}</td>
                  <td>
                    <form action="/suratmasuk/{{$value->id}}" method="POST">
                      <a href="/suratmasuk/{{$value->id}}" class="btn btn-info">Show</a>
                      <a href="/suratmasuk/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                      
                          @csrf
                          @method('DELETE')
                          <input type="submit" class="btn btn-danger my-1" value="Delete">
                      </form>
                  </td>
              </tr>
          @empty
              <tr colspan="3">
                  <td>No data</td>
              </tr>  
          @endforelse              
      </tbody>
  </table>
  </div>
@endsection
@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Masuk ke Halaman Surat Masuk",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endpush
