@extends('layout.master')

@section('content')
<div class="mt-5 ml-5">
    <h2>Show Data Surat Masuk {{$suratmasuk->id}}</h2>
    <p>{{$suratmasuk->no_surat}}</p>
    <p>{{$suratmasuk->asal_surat}}</p>
    <p>{{$suratmasuk->kode_surat}}</p>
    <p>{{$suratmasuk->isi}}</p>
    <p>{{$suratmasuk->tanggal_surat}}</p>
    <p>{{$suratmasuk->tanggal_terima}}</p>
</div>
@endsection