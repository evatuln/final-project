<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'SuratMasukController@index');

    
    // SURAT MASUK
    Route::get('/suratmasuk', 'SuratMasukController@index'); //menampilkan halaman tabel suratmasuk

    Route::get('/suratmasuk/create', 'SuratMasukController@create'); //menampilkan form untuk membuat data baru surat masuk

    Route::post('/suratmasuk', 'SuratMasukController@store'); //menyimpan data surat masuk baru

    Route::get('/suratmasuk/{suratmasuk_id}', 'SuratMasukController@show'); //menampilkan data surat masuk

    Route::get('/suratmasuk/{suratmasuk_id}/edit', 'SuratMasukController@edit'); //menampilkan form untuk edit data-data suratmasuk

    Route::put('/suratmasuk/{suratmasuk_id}', 'SuratMasukController@update'); //menyimpan data surat masuk yang sudah diedit melalui form edit suratmasuk

    Route::delete('/suratmasuk/{suratmasuk_id}', 'SuratMasukController@destroy'); //menghapus data suratmasuk dengan id tertentu


    // SURAT KELUAR
    Route::get('/suratkeluar', 'SuratKeluarController@index');

    Route::get('/suratkeluar/create', 'SuratKeluarController@create'); 

    Route::post('/suratkeluar', 'SuratKeluarController@store');

    Route::get('/suratkeluar/{suratkeluar_id}', 'SuratKeluarController@show');

    Route::get('/suratkeluar/{suratkeluar_id}/edit', 'SuratKeluarController@edit');

    Route::put('/suratkeluar/{suratkeluar_id}', 'SuratKeluarController@update');

    Route::delete('/suratkeluar/{suratkeluar_id}', 'SuratKeluarController@destroy');


    // // INSTANSI
    Route::resource('instansi', 'InstansiController');

    //PROFIL
    Route::resource('profil', 'ProfilController');

    // // Route Upload File
    // Route::get('/upload', 'UploadController@upload');
    // Route::post('/upload/proses', 'UploadController@proses_upload');


});

Auth::routes();



// Route::get('/instansi', 'InstansiController@index');

// Route::get('/instansi/create', 'InstansiController@create'); 

// Route::post('/instansi', 'InstansiController@store');

// Route::get('/instansi/{instansi_id}', 'InstansiController@show');

// Route::get('/instansi/{instansi_id}/edit', 'InstansiController@edit');

// Route::put('/instansi/{instansi_id}', 'InstansiController@update');

// Route::delete('/instansi/{instansi_id}', 'InstansiController@destroy');


// //KLASIFIKASI
// Route::get('/klasifikasi', 'KlasifikasiController@index');

// Route::get('/klasifikasi/create', 'KlasifikasiController@create'); 

// Route::post('/klasifikasi', 'KlasifikasiController@store');

// Route::get('/klasifikasi/{klasifikasi_id}', 'KlasifikasiController@show');

// Route::get('/klasifikasi/{klasifikasi_id}/edit', 'KlasifikasiController@edit');

// Route::put('/klasifikasi/{klasifikasi_id}', 'KlasifikasiController@update');

// Route::delete('/klasifikasi/{klasifikasi_id}', 'KlasifikasiController@destroy');



